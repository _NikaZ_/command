﻿using System.Collections.Generic;
using Commands;
using UnityEngine;

public class CommandProcessor : MonoBehaviour
{
    private Stack<ICommand> _commandHistory;

    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Sprite _sprite1;
    [SerializeField] private Sprite _sprite2;
    [SerializeField] private Sprite _sprite3;

    private void Awake()
    {
        _commandHistory = new Stack<ICommand>();
    }

    private void Update()
    {
        Moving();
        SpriteChanging();
        ColorChanging();
        Undo();
    }

    private void Undo()
    {
        if (Input.GetKeyDown(KeyCode.Backspace) && _commandHistory.Count > 0)
        {
            _commandHistory.Pop().Undo();
        }
            
    }

    private void ColorChanging()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Color color = Random.ColorHSV(0, 1, 0, 1, 0, 1);
            ChangeColor changeColor = new ChangeColor(_spriteRenderer, color);
            changeColor.Execute();
            _commandHistory.Push(changeColor);
        }
    }

    private void SpriteChanging()
    {
        ChangeSprite changeSprite;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            changeSprite = new ChangeSprite(_spriteRenderer, _sprite1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            changeSprite = new ChangeSprite(_spriteRenderer, _sprite2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            changeSprite = new ChangeSprite(_spriteRenderer, _sprite3);
        }
        else
        {
            return;
        }
            
        changeSprite.Execute();
        _commandHistory.Push(changeSprite);
    }

    private void Moving()
    {
        Move move;
        if (Input.GetKeyDown(KeyCode.W))
        {
            move = new Move(transform, Vector3.up);
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            move = new Move(transform, Vector3.down);
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            move = new Move(transform, Vector3.left);
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            move = new Move(transform, Vector3.right);
        }
        else
        {
            return;
        }
        move.Execute();
        _commandHistory.Push(move);
    }
}