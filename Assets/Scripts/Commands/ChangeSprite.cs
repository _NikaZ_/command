﻿using UnityEngine;

namespace Commands
{
    public class ChangeSprite : ICommand
    {
        private readonly SpriteRenderer _spriteRenderer;
        private readonly Sprite _sprite;
        private Sprite _startSprite;

        public ChangeSprite(SpriteRenderer spriteRenderer, Sprite sprite)
        {
            _spriteRenderer = spriteRenderer;
            _sprite = sprite;
        }

        public void Execute()
        {
            _startSprite = _spriteRenderer.sprite;
            _spriteRenderer.sprite = _sprite;
        }

        public void Undo()
        {
            _spriteRenderer.sprite = _startSprite;
        }
    }
}