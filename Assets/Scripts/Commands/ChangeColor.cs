﻿using UnityEngine;

namespace Commands
{
    public class ChangeColor : ICommand
    {
        private readonly SpriteRenderer _spriteRenderer;
        private readonly Color _color;
        private Color _startColor;

        public ChangeColor(SpriteRenderer spriteRenderer, Color color)
        {
            _spriteRenderer = spriteRenderer;
            _color = color;
        }


        public void Execute()
        {
            _startColor = _spriteRenderer.color;
            _spriteRenderer.color = _color;
        }

        public void Undo()
        {
            _spriteRenderer.color = _startColor;
        }
    }
}