﻿using UnityEngine;

namespace Commands
{
    public class Move : ICommand
    {
        private readonly Transform _transform;
        private readonly Vector3 _direction;

        public Move(Transform transform, Vector3 vector3)
        {
            _transform = transform;
            _direction = vector3;
        }

        public void Execute()
        {
            _transform.Translate(_direction);
        }

        public void Undo()
        {
            _transform.Translate(-_direction);
        }
    }
}